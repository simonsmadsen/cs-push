```const fetch = require('node-fetch') // only with node
const csPush = require('cs-push')

// Init
const push = csPush(fetch,'your token')
// Handle actions
push.onAction((action, params, isOwn) => {
  console.log(action,params, isOwn)
})
// Handle status changes
// connecting, connected, disconnected, reconnecting, connect
push.onStatusChange(status => {
  console.log(status)
})
// Send action
const actionId = 1
const param1 = 23
const param2 = 55
push.pushAction(actionId, param1, param2)
```