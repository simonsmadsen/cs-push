const io = require('socket.io-client')

const makeNumberId = () => {
  let text = ''
  const possible = '0123456789'

  for (let i = 0; i < 15; i++) text += possible.charAt(Math.floor(Math.random() * possible.length))

  return text.toString()
}

const connectSocket = url =>
  new Promise((res, err) => {
    const socket = io(url, {
      reconnection: true,
      reconnectionDelay: 1000,
      reconnectionDelayMax: 5000,
      reconnectionAttempts: Infinity,
    })
    socket.on('connect', () => {
      res(socket)
    })
  })

module.exports = (fetch, key) => {
  const _onActionListeners = {}
  let _onAction = (...x) => {
    Object.values(_onActionListeners).forEach(func => func(...x))
  }
  let _status = []
  const _onStatusChangeListeners = {
    '12313': x => {
      _status = x
    },
  }
  let _onStatusChange = y => {
    Object.values(_onStatusChangeListeners).forEach(x => x(y))
  }
  let _servers = []
  let _name = ''
  let _socket = {
    id: '',
    connected: false,
  }

  const init = async () => {
    _onStatusChange('connecting')
    const respond = await fetch(`https://zt2630zwp5.execute-api.eu-west-1.amazonaws.com/dev/connection/get/${key}`)
    const { success, name, error, myServer, servers } = await respond.json()
    if (!success) throw error
    _socket = await connectSocket(myServer)
    _name = name
    _servers = servers
      .split(',')
      .filter(x => x)
      .map(x => x.trim())
    _onStatusChange('connected')
    _socket.on('msg', x => {
      console.log(x)
      const [sender, action, params] = x
        .replace('"', '')
        .replace('"', '')
        .split('|')
      _onAction(action, params.split(',').filter(x => x), sender === _socket.id.replace('#', ''))
    })
    _socket.on('disconnect', () => {
      _onStatusChange('disconnected')
    })
    _socket.on('reconnecting', () => {
      _onStatusChange('reconnecting')
    })
    _socket.on('connect', () => {
      _onStatusChange('connect')
    })
  }
  setTimeout(init, 200)

  return {
    getSocket: () => _socket,
    getStatus: () => _status,
    onAction: x => {
      _onAction = x
    },
    onStatusChange: x => {
      _onStatusChange = x
    },
    addStatusChangeListener: func => {
      const number = makeNumberId()
      _onStatusChangeListeners[number] = func
      return number
    },
    removeStatusChangeListiner: id => {
      delete _onStatusChangeListeners[id]
    },
    addActionListiner: func => {
      const number = makeNumberId()
      _onActionListeners[number] = func
      return number
    },
    removeActionListiner: id => {
      delete _onActionListeners[id]
    },
    pushAction: (action, ...params) => {
      if (!_socket) return
      _servers.forEach(x => {
        const uri = `${x}/${_name}?action=${_socket.id.replace('#', '')}|${action}|${params.join(',')}`
        console.log(uri)
        fetch(uri, {
          method: 'POST',
        })
          .then('sendt')
          .catch(`${x} : server not responding.`)
      })
    },
  }
}
